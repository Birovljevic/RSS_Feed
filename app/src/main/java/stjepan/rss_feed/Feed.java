package stjepan.rss_feed;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Stjepan on 7.11.2017..
 */

@Root(name = "rss", strict = false)
public class Feed {

    @Element(name = "channel") private Channel mChannel;

    public Feed(){

    }
    public Feed(Channel mChannel) {
        this.mChannel = mChannel;
    }

    public Channel getmChannel() {
        return mChannel;
    }
}

package stjepan.rss_feed;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnTextChanged;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class MainActivity extends Activity implements Callback<Feed> {

    @BindView(R.id.lvFeedItems) ListView lvFeedItems;
    @BindView(R.id.etCategorySearch) EditText etCategorySearch;
    @BindView(R.id.btnSearch) Button btnSearchCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.showNews();
    }

    private void showNews() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BugAPI.BASE_URL)
                .client(new OkHttpClient())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();

        BugAPI api = retrofit.create(BugAPI.class);
        Call<Feed> request = api.getNews();
        request.enqueue(this);
    }

    /* --- ne ponasa se zeljeno
    @OnTextChanged(R.id.etCategorySearch)
    public void onCategorySearchChange(Editable editable){
        showNews();
    }
     */

    @Override
    public void onResponse(Call<Feed> call, Response<Feed> response) {
        String categoryQuery = "";
        try{
            categoryQuery = this.etCategorySearch.getText().toString();
            categoryQuery.toLowerCase();
        }catch (Exception e){
            e.printStackTrace();
        }

        Channel feed = response.body().getmChannel();
        List<FeedItem> items = feed.getmFeedItemList();

        if (!categoryQuery.equals("")){
            for (int i = 0; i < items.size(); i++){
                if (!items.get(i).getmCategory().toLowerCase().equals(categoryQuery)){
                    items.remove(i);
                    i--;
                }
            }
        }

        RSSAdapter adapter = new RSSAdapter(items);
        lvFeedItems.setAdapter(adapter);
    }

    @Override
    public void onFailure(Call<Feed> call, Throwable t) {
        Log.e("Fail", t.getMessage());
        Toast.makeText(this, "ERROR", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btnSearch)
    public void categorySearch(){
        showNews();
    }
    @OnItemClick(R.id.lvFeedItems)
    public void onItemClick(int position)
    {
        FeedItem feedItem = (FeedItem) this.lvFeedItems.getAdapter().getItem(position);
        Uri data = Uri.parse(feedItem.getmLink());
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(data);
        if(canBeCalled(intent))
        {
            startActivity(intent);
        }
    }

    private boolean canBeCalled(Intent intent) {
        PackageManager pm = this.getPackageManager();
        if(intent.resolveActivity(pm)==null)
        {
            return  false;
        }
        else {
            return true;
        }
    }
}

package stjepan.rss_feed;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Stjepan on 7.11.2017..
 */
@Root(name = "enclosure", strict = false)
public class LinkThumbnail {

    @Attribute(name = "url") private String mUrl;

    public LinkThumbnail(){

    }
    public LinkThumbnail(String mUrl) {
        this.mUrl = mUrl;
    }

    public String getmUrl() {
        return mUrl;
    }
}

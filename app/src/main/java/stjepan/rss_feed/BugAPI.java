package stjepan.rss_feed;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Stjepan on 7.11.2017..
 */

public interface BugAPI {
    String BASE_URL = "http://www.bug.hr/";
    @GET("rss/vijesti")
    Call<Feed> getNews();
}

package stjepan.rss_feed;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Stjepan on 7.11.2017..
 */

public class RSSAdapter extends BaseAdapter{
    List<FeedItem> mFeedItemList;

    public RSSAdapter(List<FeedItem> mFeedItemList) {
        this.mFeedItemList = mFeedItemList;
    }
    @Override
    public int getCount() {
        return this.mFeedItemList.size();
    }

    @Override
    public Object getItem(int postion) {
        return this.mFeedItemList.get(postion);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FeedItemHolder holder;
        if (convertView == null){
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
            holder = new FeedItemHolder(convertView);
            convertView.setTag(holder);
        }
        else{
            holder = (FeedItemHolder) convertView.getTag();
        }
        FeedItem feedItem = this.mFeedItemList.get(position);
        holder.tvTitle.setText(feedItem.getmTitle());
        holder.tvPubDate.setText(feedItem.getmPubDate());
        holder.tvCategory.setText(feedItem.getmCategory());
        holder.tvDescription.setText(feedItem.getmDescription());

        Picasso.with(parent.getContext())
                .load(feedItem.getmThumbnail()
                .getmUrl())
                .fit()
                .centerCrop()
                .error(R.drawable.image_not_found)
                .placeholder(R.drawable.image_placeholder)
                .into(holder.ivThumbnail);

        return convertView;
    }

    static class FeedItemHolder{
        @BindView(R.id.ivThumbnail) ImageView ivThumbnail;
        @BindView(R.id.tvTitle) TextView tvTitle;
        @BindView(R.id.tvPubDate) TextView tvPubDate;
        @BindView(R.id.tvCategory) TextView tvCategory;
        @BindView(R.id.tvDescription) TextView tvDescription;

        public FeedItemHolder(View view) {
            ButterKnife.bind(this, view);}
    }
}

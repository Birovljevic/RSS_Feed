package stjepan.rss_feed;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Stjepan on 7.11.2017..
 */
@Root(name = "channel", strict = false)
public class Channel {

    @ElementList(inline = true, name = "item") private List<FeedItem> mFeedItemList;

    public Channel(){

    }
    public Channel(List<FeedItem> mFeedItemList) {
        this.mFeedItemList = mFeedItemList;
    }

    public List<FeedItem> getmFeedItemList() {
        return mFeedItemList;
    }
}

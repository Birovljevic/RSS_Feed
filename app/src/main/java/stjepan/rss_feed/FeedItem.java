package stjepan.rss_feed;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Stjepan on 7.11.2017..
 */
@Root(name = "item", strict = false)
public class FeedItem {

    @Element(name = "link") private String mLinkMore;
    @Element(name = "category") private String mCategory;
    @Element(name = "title") private String mTitle;
    @Element(name="description") private String mDescription;
    @Element(name = "pubDate") private String mPubDate;
    @Element(name = "enclosure") private LinkThumbnail mLinkThumbnail;

    //region Constructors
    public FeedItem(){

    }
    public FeedItem(String mMore, String mCategory, String mTitle, String mDescription, String mPubDate, LinkThumbnail mLinkThumbnail) {
        this.mLinkMore = mMore;
        this.mCategory = mCategory;
        this.mTitle = mTitle;
        this.mDescription = mDescription;
        this.mPubDate = mPubDate;
        this.mLinkThumbnail = mLinkThumbnail;
    }
    //endregion

    //region Getters
    public String getmLink() {
        return mLinkMore;
    }

    public String getmCategory() {
        return mCategory;
    }

    public String getmTitle() {
        return mTitle;
    }

    public String getmDescription() {
        return mDescription;
    }

    public String getmPubDate() {
        return mPubDate;
    }

    public LinkThumbnail getmThumbnail() {
        return mLinkThumbnail;
    }
    //endregion
}
